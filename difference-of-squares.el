;;; difference-of-squares.el --- Difference of Squares (exercism)

;;; Commentary:

;;; Code:
(defun square-of-sum
    (n)
  "Calculate the square of the sum of integers from 1 to n inclusive"
  (let ((sum 0)
	(i 1))
      (while (<= i n)
	(setq sum (+ sum i))
	(setq i (1+ i)))
      (expt sum 2)))

(defun sum-of-squares
    (n)
  "Calculate the sum of the square of integers from 1 to n inclusive"
  (let ((sum 0)
	(i 1))
      (while (<= i n)
	(setq sum (+ sum (expt i 2)))
	(setq i (1+ i)))
      sum))

(defun difference
    (n)
  "Calculate the difference of the square of sum and the sum of squares of inteegers from 1 to n inclusive"
  (- (square-of-sum n) (sum-of-squares n)))

(provide 'difference-of-squares)
;;; difference-of-squares.el ends here
